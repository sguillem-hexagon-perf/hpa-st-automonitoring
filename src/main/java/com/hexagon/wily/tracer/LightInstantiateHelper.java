package com.hexagon.wily.tracer;

import com.wily.util.feedback.IModuleFeedbackChannel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LightInstantiateHelper {

   private static Map<String, Method> cacheMethod = new ConcurrentHashMap();
   private static Map<String, Field> cacheField = new ConcurrentHashMap();

   private static Method getMethod(Class c, String method, Object... args) throws NoSuchMethodException {
      Method m = cacheMethod.get(c.getName() + method);
      if (m == null) {
         // Build args class
         Class[] argClass = new Class[args.length];
         int i = 0;
         for (Object arg : args) {
            argClass[i++] = arg.getClass();
         }

         try {
            m = c.getMethod(method, argClass);
         } catch (NoSuchMethodException e) {
            Class<?> clazz = c;
            while ((clazz != null) && (m == null)) {
               try {
                  m = clazz.getDeclaredMethod(method, argClass);
               }catch (NoSuchMethodException e1) {
                  // go to next super class
                  clazz = clazz.getSuperclass();
               }
            }
            if (m == null) {
               return null;
            }
         }
         m.setAccessible(true);
         cacheMethod.put(c.getName() + method, m);
      }

      return m;
   }

   public static Object getValueByStaticMethod(Class c, String method, IModuleFeedbackChannel log, Object... args) {
      if (c == null) {
         return null;
      } else {
         return getValueByMethodAndClass(c, null, method, log, args);
      }
   }

   public static Object getValueByMethod(Object o, String method, IModuleFeedbackChannel log, Object... args) {
      if (o == null) {
         return null;
      } else {
         Class c = o.getClass();
         return getValueByMethodAndClass(c, o, method, log, args);
      }
   }

   private static Object getValueByMethodAndClass(Class c, Object o, String method, IModuleFeedbackChannel log, Object... args) {
      try {
         Method m = getMethod(c, method, args);
         if (m == null) {
            log.warn("InstantiateHelper.getValueByMethod, method not found " + method + " on " + o.getClass());
            return null;
         }
         return m.invoke(o, args);
      } catch (Exception var4) {
         log.warn("InstantiateHelper.getValueByMethod, error " + var4.getMessage(), var4);
         return null;
      }
   }


   public static Object getValueByField(Object o, String field, IModuleFeedbackChannel log) {
      if (o == null) {
         return null;
      } else {
         Class c = o.getClass();
         Field f = getField(field, c);
         return getValueByField(o, f, log);
      }
   }

   private static Object getValueByField(Object o, Field f, IModuleFeedbackChannel log) {
      if (f == null) {
         log.warn("InstantiateHelper.getValueByField, field not found " + f + " on " + o.getClass());
         return null;
      }
      try {
         return f.get(o);
      } catch (IllegalAccessException e) {
         return null;
      }
   }

   private static Field getField(String field, Class c) {
      // TODO : performance OPTIMIZE (string concat)
      Field f = cacheField.get(c.getName() + field);
      if (f == null) {
         try {
            f = c.getField(field);
         } catch (NoSuchFieldException e) {
            Class<?> clazz = c;
            while ((clazz != null) && (f == null)) {
               try {
                  f = clazz.getDeclaredField(field);
               }catch (NoSuchFieldException e1) {
                  // go to next super class
                  clazz = clazz.getSuperclass();
               }
            }
            if (f == null) {
               return null;
            }
         }
         f.setAccessible(true);
         cacheField.put(c.getName() + field, f);
      }
      return f;
   }

}
