package com.hexagon.wily.tracer;

import com.wily.introscope.agent.IAgent;
import com.wily.introscope.agent.trace.*;
import com.wily.util.feedback.IModuleFeedbackChannel;
import com.wily.util.feedback.Module;
import com.wily.util.feedback.ModuleFeedbackChannelWrapper;
import com.wily.util.properties.AttributeListing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ScheduledTaskAutoMonitoringTracer extends ASingleInstanceTracerFactory implements ITracer {

    public static final String JVM_SETTINGS = "JVM_SETTINGS";
    public static final String AUTOMATIC_MONITORED_BY_HPA = "AutomaticMonitoredByHpa";
    private static List<String> lastConfiguration;
    private static long lastConfigurationLoad;
    public static final int RELOAD_CHECK = 2 * 60 * 1000;
    public static final String HPA_MONITOR_ALL = "HPA_MONITOR_ALL";

    private IModuleFeedbackChannel log;

    public ScheduledTaskAutoMonitoringTracer(IAgent agent, AttributeListing parameters, ProbeIdentification probe, Object sampleTracedObject) {
        super(agent, parameters, probe, sampleTracedObject);
        log = new ModuleFeedbackChannelWrapper(agent.IAgent_getModuleFeedback(), new Module("HPA ST automonitoring"));
    }


    /*
     * Task are executed from com.calypso.apps.scheduling.DefaultJob#executeJob
     *
     * A modifier :
     * 1) prendre la task en paramètre,
     * 2) faire le getAttribute/setAttribute sur JVM_SETTINGS.
     *
     */



    @Override
    public void ITracer_startTrace(int i, InvocationData invocationData) {


    // com.calypso.apps.scheduling.DefaultJob#executeJob(JobExecutionContext context, ScheduledTask task)
        Object scheduledTask = (Object) invocationData.getInvocationParameterAsObject(1);
        try {
            addHpaJvmSettings(scheduledTask, log);
        } catch (IOException e) {
            log.warn("Error while add JVM parameters", e);
        }
    }


    /**
     *
     * @param scheduledTask com.calypso.tk.util.ScheduledTask
     * @param log
     */
    void addHpaJvmSettings(Object scheduledTask, IModuleFeedbackChannel log) throws IOException {
        // Do not use getAttributes here or infinite loop...

        String externalRef = (String) LightInstantiateHelper.getValueByMethod(scheduledTask, "getExternalReference", log);
        String errorMessage = "Can not add HPA monitoring to JVM settings for externalRef=" + externalRef + ", task=" + scheduledTask.toString();
        if (externalRef == null) {
            log.warn(errorMessage);
            return;
        }
        String currentJvmSettings = (String) LightInstantiateHelper.getValueByMethod(scheduledTask, "getAttribute", log, JVM_SETTINGS);
        if (currentJvmSettings != null && currentJvmSettings.indexOf("introscope.agent.customProcessName")>0) {
            log.warn(errorMessage + ", system property introscope.agent.customProcessName already set");
            return;
        }

        String schedulerAgentProfile = System.getProperty("com.wily.introscope.agentProfile");
        if (schedulerAgentProfile == null) {
            log.warn(errorMessage + ", system property com.wily.introscope.agentProfile is null");
            return;
        }
        Path schedulerAgentProfilePath = Paths.get(schedulerAgentProfile);
        if (!Files.exists(schedulerAgentProfilePath)) {
            log.warn(errorMessage + ", file does not exists = " + schedulerAgentProfile);
            return;
        }
        // Up to level should be in wily : wily/core/config/*.profile => wily/
        Path agentPath = schedulerAgentProfilePath.getParent().getParent().getParent();

        String profile = getProfileForAuthorizedST(agentPath, externalRef, log, errorMessage);
        if (profile == null) {
            log.warn(errorMessage + ", scheduled task not authorized");
            return;
        }
        String agentName = "ScheduledTask-" + cleanExternalRef(externalRef);
        String automatedMonitored = (String) LightInstantiateHelper.getValueByMethod(scheduledTask, "getAttribute", log, AUTOMATIC_MONITORED_BY_HPA);
        if (automatedMonitored != null) {
            log.warn(errorMessage + ", automatic monitoring already added");
            return;
        }
        LightInstantiateHelper.getValueByMethod(scheduledTask, "setAttribute", log, AUTOMATIC_MONITORED_BY_HPA,  "true");
        String agentManager = System.getProperty("agentManager");
        String customProcessName = System.getProperty("introscope.agent.customProcessName");
        StringBuilder builder = new StringBuilder();
        /*
         * /!\ Do not put a space prefix for the first argument others ST Calypso does not start !!!
         */
        builder.append("-DagentManager=").append(agentManager);
        builder.append(" -Dintroscope.agent.customProcessName=").append(customProcessName);
        builder.append(" -Dintroscope.agent.agentName=").append(agentName);
        builder.append(" -Dcom.wily.introscope.agentProfile=").append(agentPath).append(File.separator).append("core").append(File.separator).append("config").append(File.separator).append("IntroscopeAgent_").append(profile).append(".profile");
        builder.append(" -Dintroscope.autoprobe.logfile=../../logs/AutoProbe-").append(agentName).append(".log");
        builder.append(" -javaagent:").append(agentPath).append("/Agent.jar");
        builder.append(" ").append(currentJvmSettings);
        LightInstantiateHelper.getValueByMethod(scheduledTask, "setAttribute", log, JVM_SETTINGS,  builder.toString());
        log.warn("Added HPA monitoring to JVM settings for externalRef=" + externalRef + ", agentName="+agentName+", task=" + scheduledTask.toString() + ", new settings = " + builder.toString());
    }

    private String cleanExternalRef(String externalRef) {
        return externalRef.replaceAll("[^a-zA-Z0-9_-]", "_");

    }

    private String getProfileForAuthorizedST(Path agentPath, String externalRef, IModuleFeedbackChannel log, String errorMessage) throws IOException {
        Path configurationPath = Paths.get(agentPath.getParent().toString(), "automatic_st_monitoring.list");
        if (!Files.exists(configurationPath)) {
            log.warn(errorMessage + ", configuration file not found = " + configurationPath.toString());
            return null;
        }
        long elapsed = System.currentTimeMillis() - lastConfigurationLoad;
        // Reload every 2mn
        //log.info("elapsed = " + elapsed);
        if (elapsed > (RELOAD_CHECK)) {
            lastConfigurationLoad = System.currentTimeMillis();
            lastConfiguration = Files.readAllLines(configurationPath);
            //log.info("reload = " + elapsed);
        }
        if (lastConfiguration != null) {
            for (String line : lastConfiguration) {
                if (line != null && line.trim().equals("")) {
                    continue;
                }
                if (line.startsWith("//")) {
                    continue;
                }
                String[] tokens = splitProfile(configurationPath, line, log);
                if (tokens == null) {
                    continue;
                }
                String externalRefPattern = tokens[0];
                String profile = tokens[1];
                if (externalRefPattern.equals(HPA_MONITOR_ALL)) {
                    return profile;
                }
                if (externalRef.matches(externalRefPattern)) {
                    return profile;
                }
            }
        }
        return null;
    }

    private String[] splitProfile(Path configurationPath, String line, IModuleFeedbackChannel log) {
        String[] tokens = line.split(";;");
        if (tokens != null && tokens.length == 2) {
            return tokens;
        }
        log.warn("bad format in " + configurationPath.toString() + " for " + line);
        return null;
    }

    @Override
    public void ITracer_finishTrace(int i, InvocationData invocationData) {

    }

    @Override
    public ReentrancyLevel ITracerFactory_getReentrancyLevel() {
        return ReentrancyLevel.kNone;
    }

    @Override
    public boolean ITracerFactory_isShutoff() {
        return false;
    }
}



