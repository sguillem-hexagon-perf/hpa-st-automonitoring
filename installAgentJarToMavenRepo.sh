#!/bin/bash

rm -rf ~/.m2/repository/com/ca/agent/javaagent

mvn install:install-file -DcreateChecksum=true -Dfile=./lib/Agent.jar -DgroupId=com.ca.agent -DartifactId=javaagent -Dversion=2022.3.0.38 -Dpackaging=jar -DlocalRepositoryPath=local-maven-repo
